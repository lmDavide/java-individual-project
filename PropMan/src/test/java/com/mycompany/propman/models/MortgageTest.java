/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.propman.models;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author david
 */
public class MortgageTest {
    
    public MortgageTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    
    /**
     * Test of getAmount method, of class Mortgage.
     */
    @org.junit.jupiter.api.Test
    public void testGetAmount() {
        System.out.println("getAmount");
        Mortgage mortgage = new Mortgage("102","101", "RBC", 15, 500000, 2.98);
        double expResult = 500000;
        double result = mortgage.getAmount();
        assertEquals(expResult, result);
    } 

    /**
     * Test of getPropertyID method, of class Mortgage.
     */
    @org.junit.jupiter.api.Test
    public void testGetPropertyID() {
        System.out.println("getPropertyID");
        Mortgage mortgage = new Mortgage("102","101","RBC", 15, 500000, 2.98);
        String expResult = "102";
        String result = mortgage.getPropertyID();
        assertEquals(expResult, result);
    } 

    
    /**
     * Test of getAmount method, of class Mortgage.
     */
    @org.junit.jupiter.api.Test
    public void testGetBank() {
        System.out.println("getAmount");
        Mortgage mortgage = new Mortgage("102","101","RBC", 15, 500000, 2.98);
        String expResult = "RBC";
        String result = mortgage.getBank();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLoanTerm method, of class Mortgage.
     */
    @org.junit.jupiter.api.Test
    public void testGetLoanTerm() {
        System.out.println("getLoanTerm");
        Mortgage mortgage = new Mortgage("102","101", "RBC", 15, 500000, 2.98);
        int expResult = 15;
        int result = mortgage.getLoanTerm();
        assertEquals(expResult, result);
    }

    /**
     * Test of getInterestRate method, of class Mortgage.
     */
    @org.junit.jupiter.api.Test
    public void testGetInterestRate() {
        System.out.println("getInterestRate");
        Mortgage mortgage = new Mortgage("102","101","RBC", 15, 500000, 2.98);
        double expResult = 2.98;
        double result = mortgage.getInterestRate();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getMonthlyInterest method, of class Mortgage.
     */
    @org.junit.jupiter.api.Test
    public void testGetMonthlyInterest() {
        System.out.println("getMonthlyInterest");
        Mortgage mortgage = new Mortgage("102","101","RBC", 15, 500000, 2.98);
        double expResult = 1990000.0;
        double result = mortgage.getMonthlyPayment();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setPropertyID method, of class Mortgage.
     */
    @org.junit.jupiter.api.Test
    public void testSetPropertyID() {
        System.out.println("setPropertyID");
        String property_id = "102";
        Mortgage mortgage = new Mortgage("102","101","RBC", 15, 500000, 2.98);
        mortgage.setPropertyID(property_id);
    }

    /**
     * Test of setBank method, of class Mortgage.
     */
    @org.junit.jupiter.api.Test
    public void testSetBank() {
        System.out.println("setBank");
        String bank = "BMO";
        Mortgage mortgage = new Mortgage("102","101","RBC", 15, 500000, 2.98);
        mortgage.setBank(bank);
    }

    /**
     * Test of setLoanTerm method, of class Mortgage.
     */
    @org.junit.jupiter.api.Test
    public void testSetLoanTerm() {
        System.out.println("setLoanTerm");
        int loan_term = 30;
        Mortgage mortgage = new Mortgage("102","101","RBC", 15, 500000, 2.98);
        mortgage.setLoanTerm(loan_term);
    }

    /**
     * Test of setAmount method, of class Mortgage.
     */
    @org.junit.jupiter.api.Test
    public void testSetAmount() {
        System.out.println("setAmount");
        double amount = 1000000;
        Mortgage mortgage = new Mortgage("102","101","RBC", 15, 500000, 2.98);
        mortgage.setAmount(amount);
    }

    /**
     * Test of setInterestRate method, of class Mortgage.
     */
    @org.junit.jupiter.api.Test
    public void testSetInterestRate() {
        System.out.println("setInterestRate");
        double interest_rate = 3.00;
        Mortgage mortgage = new Mortgage("102","101","RBC", 15, 500000, 2.98);
        mortgage.setInterestRate(interest_rate);
    }
    
}
