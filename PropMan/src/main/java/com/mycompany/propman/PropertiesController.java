/**
 * Controller for Properties View (properties_main)
 * @author David Tran - 1938381
 */

package com.mycompany.propman;

// Imports
import com.mycompany.propman.models.Property;
import com.mycompany.propman.datacontroller.addcontroller.AddPropertyController;
import com.mycompany.propman.datacontroller.updatecontroller.UpdatePropertyController;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;


import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;



public class PropertiesController extends MainController implements Initializable{
    
    // Fields
    @FXML public TableView<Property> propertyTable;    
    
    @FXML public TableColumn<Property, String> property_id;
    @FXML public TableColumn<Property, String> property_type;
    @FXML public TableColumn<Property, String> property_address;
    @FXML public TableColumn<Property, Integer> property_units;
    @FXML public TableColumn<Property, Double> property_fee;
    @FXML public TableColumn<Property, Boolean> property_full;
    
    public ObservableList<Property> observableList;
    public List<Property> list;
    

    /**
     * addProperty Method that gets called when Add Property Button is clicked
     * @throws IOException 
     */
    @FXML 
    @Override
    public void add() throws IOException { 
        AddPropertyController controller = new AddPropertyController();
        
        super.newView(this.propertyTable, controller, "/com/mycompany/propman/add_property.fxml", "Add Property", 450);
    }
    
    /**
     * updateProperty Method that gets called when Add Property Button is clicked
     * @throws IOException 
     */
    @FXML 
    @Override
    public void update() throws IOException { 
        
        if(!propertyTable.getSelectionModel().isEmpty()){
            Property selectedProperty = propertyTable.getSelectionModel().getSelectedItem();


            UpdatePropertyController controller = new UpdatePropertyController();

            super.newView(this.propertyTable, controller, "/com/mycompany/propman/update_property.fxml", "Update Property", 450);

            controller.setObject(selectedProperty);
            controller.showData();
        }
    }
    
    /**
     * removeProperty Method that gets called when Remove Property Button is clicked
     * @throws IOException 
     * @throws java.sql.SQLException 
     */
    @FXML
    @Override
    public void remove() throws IOException, SQLException{
        if(!propertyTable.getSelectionModel().isEmpty()){
            Property selectedProperty = propertyTable.getSelectionModel().getSelectedItem();
            String removed_id = selectedProperty.getPropertyID() + "";

            super.removeData("Property", selectedProperty, propertyTable, "propman_property", "property_id", removed_id);
        }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources){
        property_id.setCellValueFactory(new PropertyValueFactory<>("PropertyID"));
        property_type.setCellValueFactory(new PropertyValueFactory<>("Type"));
        property_address.setCellValueFactory(new PropertyValueFactory<>("Address"));
        property_units.setCellValueFactory(new PropertyValueFactory<>("Units"));
        property_fee.setCellValueFactory(new PropertyValueFactory<>("Fee"));
        property_full.setCellValueFactory(new PropertyValueFactory<>("Full"));   
        
        try {
            this.observableList = super.fetchData("property");
        } catch (SQLException ex) {
            Logger.getLogger(PropertiesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        propertyTable.setItems(this.observableList);
    }
    
}
