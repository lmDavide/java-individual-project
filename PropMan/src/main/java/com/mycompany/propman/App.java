package com.mycompany.propman;

// Imports
import com.mycompany.propman.database.DatabaseConnection;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.sql.*;


import java.io.IOException;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 * JavaFX App
 */
public class App extends Application {
        public static DatabaseConnection dbObject;
        public static Connection conn;
        private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException, SQLException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Do you wish to set up the database tables?", ButtonType.YES, ButtonType.NO);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {  
            dbObject.tablesSetup();  
        }
        
        scene = new Scene(loadFXML("main_properties"), 626, 519);
        stage.setScene(scene);
        stage.setTitle("PropMan by David Tran 1938381");
        stage.setResizable(false);
        stage.show();
        
        
        
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    
    
    public static void main(String[] args) throws SQLException{
        
        dbObject = new DatabaseConnection();
        
        dbObject.createConnection();
  
        conn = dbObject.getConnection();
        
        launch();
    }

}