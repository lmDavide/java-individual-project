package com.mycompany.propman;

// Imports
import com.mycompany.propman.models.Contractor;
import com.mycompany.propman.datacontroller.addcontroller.AddContractorController;
import com.mycompany.propman.datacontroller.updatecontroller.UpdateContractorController;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;



public class ContractorsController extends MainController implements Initializable {
    
    // Fields
    @FXML TableView<Contractor> contractorTable;
    
    @FXML TableColumn<Contractor, String> contractor_id;
    @FXML TableColumn<Contractor, String> contractor_specification;
    @FXML TableColumn<Contractor, String> contractor_company;
    @FXML TableColumn<Contractor, String> contractor_phone;
    @FXML TableColumn<Contractor, String> contractor_email;

    public ObservableList<Contractor> observableList;
    public List<Contractor> list;
    
    
    /**
     * addContractor Method that gets called when Add Contractor Button is clicked
     * @throws IOException 
     */
    @FXML 
    @Override
    public void add() throws IOException {
        AddContractorController controller = new AddContractorController();
        super.newView(this.contractorTable, controller, "/com/mycompany/propman/add_contractor.fxml", "Add Contractor", 450);
    }
    
    /**
     * updateContractor Method that gets called when Update Contractor Button is clicked
     * @throws IOException 
     */
    @FXML
    @Override
    public void update() throws IOException {
        if(!contractorTable.getSelectionModel().isEmpty()){
            Contractor selectedContractor = contractorTable.getSelectionModel().getSelectedItem();

            UpdateContractorController controller = new UpdateContractorController();

            super.newView(this.contractorTable, controller, "/com/mycompany/propman/update_contractor.fxml", "Add Contractor", 450);

            controller.setObject(selectedContractor);
            controller.showData();
        }
    }
    
    /**
     * removeContractor Method that gets called when Remove Contractor Button is clicked
     * @throws IOException 
     * @throws java.sql.SQLException 
     */
    @FXML
    @Override
    public void remove() throws IOException, SQLException {
        if(!contractorTable.getSelectionModel().isEmpty()){
            Contractor selectedContractor = contractorTable.getSelectionModel().getSelectedItem();
            String removed_id = selectedContractor.getContractorID();

            super.removeData("Contractor", selectedContractor, contractorTable, "propman_contractor", "contractor_id", removed_id);
        }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources){
        contractor_id.setCellValueFactory(new PropertyValueFactory<>("ContractorID"));
        contractor_specification.setCellValueFactory(new PropertyValueFactory<>("Specification"));
        contractor_company.setCellValueFactory(new PropertyValueFactory<>("Company"));
        contractor_phone.setCellValueFactory(new PropertyValueFactory<>("Phone"));
        contractor_email.setCellValueFactory(new PropertyValueFactory<>("Email"));

        try {
            this.observableList = fetchData("contractor");
        } catch (SQLException ex) {
            Logger.getLogger(PropertiesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        contractorTable.setItems(this.observableList);
    }
    
}
