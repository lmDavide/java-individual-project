/**
 * Superclass for the Main Controllers (PropertiesController, MortgagesController, ExpensesController, TenantsController, ContractorsController)
 * @author David Tran - 1938381
 */

package com.mycompany.propman;

// Imports
import static com.mycompany.propman.App.dbObject;
import com.mycompany.propman.datacontroller.DataController;    

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableView;
import javafx.scene.Scene;
import javafx.stage.Stage;



public abstract class MainController {
    
    /**
     * switchToProperties Method for switching to the Properties View when clicking the Properties Button
     * @throws IOException 
     */
    @FXML
    private void switchToProperties() throws IOException {
        App.setRoot("main_properties");
    }
    
    /**
     * switchToMortgages Method for switching to the Mortgages View when clicking the Mortgages Button
     * @throws IOException 
     */
    @FXML
    private void switchToMortgages() throws IOException {
        App.setRoot("main_mortgages");
    }
    
    /**
     * switchToExpenses Method for switching to the Expense View when clicking the Expenses Button
     * @throws IOException 
     */
    @FXML
    private void switchToExpenses() throws IOException {
        App.setRoot("main_expenses");
    }
    
    /**
     * switchToTenants Method for switching to the Tenants View when clicking the Tenants Button
     * @throws IOException 
     */
    @FXML
    private void switchToTenants() throws IOException {
        App.setRoot("main_tenants");
    }
    
    /**
     * switchToContractors Method for switching to the Contractors View when clicking the Contractors Button
     * @throws IOException 
     */
    @FXML
    private void switchToContractors() throws IOException {
        App.setRoot("main_contractors");
    }
    
    /**
     * newView Method that gets called from the associated add function from each main controllers
     * Creates a new scene for the controller that calls it
     * @param table
     * @param controller
     * @param fxml
     * @param title
     * @param size
     * @throws IOException 
     */
    public void newView(TableView table, DataController controller, String fxml, String title, int size) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource(fxml));
        controller.setTableView(table);
        fxmlLoader.setController(controller);
        
        Scene scene = new Scene(fxmlLoader.load(), 625, size);
        Stage stage = new Stage();
        stage.setTitle(title);
        stage.setScene(scene);
        stage.setResizable(false);
        controller.setStage(stage);
        stage.show();
    }
    
    /**
     * Creates an alert window, if click YES, it removes the data selected, if click NO, nothing happens
     * @param selected
     * @param obj
     * @param table
     * @param db_table
     * @param id_type
     * @param id_input
     * @throws SQLException 
     */
    public void removeData(String selected, Object obj, TableView table, String db_table, String id_type, String id_input) throws SQLException{
        Alert alert = new Alert(AlertType.CONFIRMATION, "Delete " + selected + " ?", ButtonType.YES, ButtonType.NO);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            table.getItems().remove(obj);
            dbObject.databaseRemove(db_table, id_type, id_input);
        }
    }
    
    /**
     * add Method for Add Button
     * @throws IOException 
     */
    public abstract void add() throws IOException;
    
    /**
     * update Method for Update Button
     * @throws IOException 
     */
    public abstract void update() throws IOException;
    
    /**
     * remove Method for Remove Button
     * @throws IOException
     * @throws SQLException 
     */
    public abstract void remove() throws IOException, SQLException;
    
    /**
     * fetchData Method to fetch data from the database into the table
     * @param type
     * @return 
     * @throws SQLException 
     */
    public ObservableList fetchData(String type) throws SQLException{
        List list = new ArrayList<>();
        
        list = App.dbObject.databaseFetch(list, type);
        
        ObservableList observableList = FXCollections.observableArrayList(list);
        
        return observableList;
    }
}