

package com.mycompany.propman.database;

// Imports
import com.mycompany.propman.models.*;
import java.util.List;
import java.sql.*;



public class DatabaseConnection {
    private Connection conn;
    
    final String createPropertyTable = "CREATE TABLE IF NOT EXISTS propman_property(" +
                                       "property_id VARCHAR(3) PRIMARY KEY, " +
                                       "type VARCHAR(11) NOT NULL, " +
                                       "address VARCHAR(30) NOT NULL, " +
                                       "units INTEGER NOT NULL, " +
                                       "fee DOUBLE NOT NULL, " +
                                       "full VARCHAR(5) NOT NULL)";
    
    final String createMortgageTable = "CREATE TABLE IF NOT EXISTS propman_mortgage(" +
                                       "mortgage_id VARCHAR(3) PRIMARY KEY, " +
                                       "property_id VARCHAR(3) NOT NULL , " +
                                       "bank VARCHAR(5) NOT NULL, " +
                                       "loan_term INTEGER NOT NULL, " +
                                       "amount DOUBLE NOT NULL, " +
                                       "interest_rate DOUBLE NOT NULL, " +
                                       "monthly_payment DOUBLE NOT NULL," +
                                       "FOREIGN KEY (property_id) REFERENCES propman_property(property_id))";
    
    final String createExpenseTable = "CREATE TABLE IF NOT EXISTS propman_expense(" +
                                       "expense_id VARCHAR(3) PRIMARY KEY, " +
                                       "property_id VARCHAR(3) NOT NULL, " +
                                       "type VARCHAR(20) NOT NULL, " +
                                       "cost DOUBLE NOT NULL," +
                                       "FOREIGN KEY (property_id) REFERENCES propman_property(property_id))";
    
    final String createTenantTable = "CREATE TABLE IF NOT EXISTS propman_tenant(" +
                                     "tenant_id VARCHAR(3) PRIMARY KEY, " +
                                     "property_id VARCHAR(3) NOT NULL, " +
                                     "unit_number INTEGER NOT NULL, " +
                                     "name VARCHAR(20) NOT NULL, " +
                                     "rent DOUBLE NOT NULL, " +
                                     "paid_amount DOUBLE NOT NULL, " +
                                     "rent_paid VARCHAR(5) NOT NULL, " +
                                     "renew VARCHAR(5) NOT NULL, " +
                                     "phone VARCHAR(20) NOT NULL, " +
                                     "email VARCHAR(50) NOT NULL," +
                                     "lease VARCHAR(200) NOT NULL, " +
                                     "FOREIGN KEY (property_id) REFERENCES propman_property(property_id))";

    final String createContractorTable = "CREATE TABLE IF NOT EXISTS propman_contractor(" +
                                         "contractor_id VARCHAR(3) PRIMARY KEY, " +
                                         "specification VARCHAR(20) NOT NULL, " +
                                         "company VARCHAR(20) NOT NULL, " +
                                         "phone VARCHAR(20) NOT NULL, " +
                                         "email VARCHAR(50) NOT NULL)";

    final String deletePropertyTable = "DROP TABLE IF EXISTS propman_property";
    final String deleteMortgageTable = "DROP TABLE IF EXISTS propman_mortgage";
    final String deleteExpenseTable = "DROP TABLE IF EXISTS propman_expense";
    final String deleteTenantTable = "DROP TABLE IF EXISTS propman_tenant";
    final String deleteContractorTable = "DROP TABLE IF EXISTS propman_contractor";
    
    
    /**
     * createConnection Method that tries to create a connection to the database
     */
    public void createConnection(){
    
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String user = "davide";
            String pass = "mysqlnowork123";
            String url = "jdbc:mysql://localhost:3306/sakila";
            this.conn = DriverManager.getConnection(url, user, pass);
            System.out.println("Database successfully connected!");
        }
        
        catch (ClassNotFoundException | SQLException e){
            System.out.println("Connection not good.");
        }
    }
    
    /**
     * Getter for the connection
     * @return 
     */
    public Connection getConnection(){
        return this.conn;
    }
    
    
    
    /**
     * tablesSetup Method for setting up the tables in the database
     * @throws SQLException 
     */
    public void tablesSetup() throws SQLException{
        executeStatement(deleteMortgageTable);
        executeStatement(deleteExpenseTable);
        executeStatement(deleteTenantTable);
        executeStatement(deleteContractorTable);
        executeStatement(deletePropertyTable);
        
        executeStatement(createPropertyTable);
        executeStatement(createMortgageTable);
        executeStatement(createExpenseTable);
        executeStatement(createTenantTable);
        executeStatement(createContractorTable);
        
    }
    
    
    
    /**
     * executeStatement Method for executing the inputted query
     * @param query
     * @throws SQLException 
     */
    private void executeStatement(String query) throws SQLException{
        try {
            PreparedStatement ps = this.conn.prepareStatement(query);

            ps.execute();
        }
        
        catch (SQLException e){
            System.out.println("There was an error.");
        }
    }
    
    
    
    
    /**
     * databaseFetch Method for fetching objects data from database
     * @param list
     * @param type
     * @return
     * @throws SQLException 
     */
    public List databaseFetch(List list, String type) throws SQLException{
        switch(type){
            case "property":
                list = fetchProperty((List<Property>) list);
                break;
            case "mortgage":
                list = fetchMortgage((List<Mortgage>) list);
                break;
            case "expense":
                list = fetchExpense((List<Expense>) list);
                break;
            case "tenant":
                list = fetchTenant((List<Tenant>) list);
                break;
            case "contractor":
                list = fetchContractor((List<Contractor>) list);
                break;
        }
        
        return list;
    }
    
    /**
     * fetchProperty Method fetches Properties data from the database
     * @param list
     * @return
     * @throws SQLException 
     */
    private List<Property> fetchProperty(List<Property> list) throws SQLException{
        
        String fetchPropertiesQuery = "SELECT * FROM propman_property";
        
        PreparedStatement ps = this.conn.prepareStatement(fetchPropertiesQuery);
        ResultSet rs = ps.executeQuery();
        
        while(rs.next()){
            String id = rs.getString(1);
            String type = rs.getString(2).toLowerCase() ;
            String address = rs.getString(3);
            int units = rs.getInt(4);
            Double fee = rs.getDouble(5);
            boolean full = Boolean.parseBoolean(rs.getString(6));
            
            switch (type) {
            case "house":
                {
                    House newProperty = new House(id, address, full);
                    list.add(newProperty);
                    break;
                }
            case "condo":
                {
                    Condo newProperty = new Condo(id, address, fee, full);
                    list.add(newProperty);
                    break;
                }
            default: // Plexes
                {
                    String plex_type = type.substring(0, 1).toUpperCase() + type.substring(1);
                    Plex newProperty = new Plex(id, plex_type, address, units, full);
                    list.add(newProperty);
                    break;
                }
            }

        }
        
        return list;
    }
    
    /**
     * fetchMortgage Method fetches Mortgages data from the database
     * @param list
     * @return
     * @throws SQLException 
     */
    private List<Mortgage> fetchMortgage(List<Mortgage> list) throws SQLException{
        
        String fetchPropertiesQuery = "SELECT * FROM propman_mortgage";
        
        PreparedStatement ps = this.conn.prepareStatement(fetchPropertiesQuery);
        ResultSet rs = ps.executeQuery();
        
        while(rs.next()){
            String mort_id = rs.getString(1);
            String prop_id = rs.getString(2);
            String bank = rs.getString(3);
            int years = rs.getInt(4);
            Double amount = rs.getDouble(5);
            Double rate = rs.getDouble(6);
            
            Mortgage newMortgage = new Mortgage(prop_id, mort_id, bank, years, amount, rate);
            
            list.add(newMortgage);
        }
        
        return list;
    }
    
    /**
     * fetchExpense Method fetches Expenses data from the database
     * @param list
     * @return
     * @throws SQLException 
     */
    private List<Expense> fetchExpense(List<Expense> list) throws SQLException{
        
        String fetchExpensesQuery = "SELECT * FROM propman_expense";
        
        PreparedStatement ps = this.conn.prepareStatement(fetchExpensesQuery);
        ResultSet rs = ps.executeQuery();
        
        while(rs.next()){
            String exp_id = rs.getString(1);
            String prop_id = rs.getString(2);
            String type = rs.getString(3);
            Double cost = rs.getDouble(4);
            
            Expense newExpense = new Expense(exp_id, prop_id, type, cost);
            
            list.add(newExpense);
        }
        
        return list;
    }
    
    /**
     * fetchTenant Method fetches Tenants data from the database
     * @param list
     * @return
     * @throws SQLException 
     */
    private List<Tenant> fetchTenant(List<Tenant> list) throws SQLException{
        
        String fetchTenantsQuery = "SELECT * FROM propman_Tenant";
        
        PreparedStatement ps = this.conn.prepareStatement(fetchTenantsQuery);
        ResultSet rs = ps.executeQuery();
        
        while(rs.next()){
            String t_id = rs.getString(1);
            String prop_id = rs.getString(2);
            int unit = rs.getInt(3);
            String name = rs.getString(4);
            Double rent = rs.getDouble(5);
            Double rent_paid = rs.getDouble(6);
            boolean renew = Boolean.parseBoolean(rs.getString(8));
            String phone = rs.getString(9);
            String email = rs.getString(10);
            String lease_path = rs.getString(11);
            
            Tenant newTenant = new Tenant(t_id, prop_id, unit, name, rent, rent_paid, renew, phone, email, lease_path);
            
            list.add(newTenant);
        }
        
        return list;
    }
    
    /**
     * fetchContractor Method fetches Contractors data from the database
     * @param list
     * @return
     * @throws SQLException 
     */
    private List<Contractor> fetchContractor(List<Contractor> list) throws SQLException{
        
        String fetchContractorsQuery = "SELECT * FROM propman_contractor";
        
        PreparedStatement ps = this.conn.prepareStatement(fetchContractorsQuery);
        ResultSet rs = ps.executeQuery();
        
        while(rs.next()){
            String id = rs.getString(1);
            String specification = rs.getString(2);
            String company = rs.getString(3);
            String phone = rs.getString(4);
            String email = rs.getString(5);
            
            Contractor newContractor = new Contractor(id, specification, company, phone, email);
            
            list.add(newContractor);
        }
        
        return list;
    }
    
    
    
    /**
     * databaseAdd Method for adding object to database
     * @param object
     * @param type
     * @throws SQLException 
     */
    public void databaseAdd(Object object, String type) throws SQLException{
        switch(type){
            case "property":
                addProperty((Property) object);
                break;
            case "mortgage":
                addMortgage((Mortgage) object);
                break;
            case "expense":
                addExpense((Expense) object);
                break;
            case "tenant":
                addTenant((Tenant) object);
                break;
            case "contractor":
                addContractor((Contractor) object);
                break;
        }
                
    }
    
    /**
     * addProperty Method to add the data from Property Object to the database
     * @param property
     * @throws SQLException 
     */
    private void addProperty(Property property) throws SQLException{
        
        String addPropertyQuery = "INSERT INTO propman_property " +
                                  "VALUES (?, ?, ?, ?, ?, ?)";
        
        PreparedStatement ps = this.conn.prepareStatement(addPropertyQuery);
        
        ps.setString(1, property.getPropertyID());
        ps.setString(2, property.getType());
        ps.setString(3, property.getAddress());
        ps.setInt(4, property.getUnits());
        ps.setDouble(5, property.getFee());
        ps.setString(6, property.getFull() + "");
        
        ps.executeUpdate();
    }
    
    /**
     * addMortgage Method to add the data from Mortgage Object to the database
     * @param mortgage
     * @throws SQLException 
     */
    private void addMortgage(Mortgage mortgage) throws SQLException{
        
        String addMortgageQuery = "INSERT INTO propman_mortgage " +
                                  "VALUES (?, ?, ?, ?, ?, ?,?)";
        
        PreparedStatement ps = this.conn.prepareStatement(addMortgageQuery);
        
        ps.setString(1, mortgage.getMortgageID());
        ps.setString(2, mortgage.getPropertyID());
        ps.setString(3, mortgage.getBank());
        ps.setInt(4, mortgage.getLoanTerm());
        ps.setDouble(5, mortgage.getAmount());
        ps.setDouble(6, mortgage.getInterestRate());
        ps.setDouble(7, mortgage.getMonthlyPayment());
        
        ps.executeUpdate();
    }
    
    /**
     * addExpense Method to add the data from Expense Object to the database
     * @param expense
     * @throws SQLException 
     */
    private void addExpense(Expense expense) throws SQLException{
        
        String addMortgageQuery = "INSERT INTO propman_expense " +
                                  "VALUES (?, ?, ?, ?)";
        
        PreparedStatement ps = this.conn.prepareStatement(addMortgageQuery);
        
        ps.setString(1, expense.getExpenseID());
        ps.setString(2, expense.getPropertyID());
        ps.setString(3, expense.getType());
        ps.setDouble(4, expense.getCost());
        
        ps.executeUpdate();
    }
    
    /**
     * addTenant Method to add the data from Tenant Object to the database
     * @param tenant
     * @throws SQLException 
     */
    private void addTenant(Tenant tenant) throws SQLException{
        
        String addMortgageQuery = "INSERT INTO propman_tenant " +
                                  "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
        PreparedStatement ps = this.conn.prepareStatement(addMortgageQuery);
        
        ps.setString(1, tenant.getTenantID());
        ps.setString(2, tenant.getPropertyID());
        ps.setInt(3, tenant.getUnitNumber());
        ps.setString(4, tenant.getTenantName());
        ps.setDouble(5, tenant.getTenantRent());
        ps.setDouble(6, tenant.getPaidAmount());
        ps.setString(7, tenant.getRentPaid()+ "");
        ps.setString(8, tenant.getTenantRenew() + "");
        ps.setString(9, tenant.getTenantPhone());
        ps.setString(10, tenant.getTenantEmail());
        ps.setString(11, tenant.getLeasePath());
        
        ps.executeUpdate();
    }
    
    /**
     * addContractor Method to add the data from Contractor Object to the database
     * @param contractor
     * @throws SQLException 
     */
    private void addContractor(Contractor contractor) throws SQLException{
        
        String addMortgageQuery = "INSERT INTO propman_contractor " +
                                  "VALUES (?, ?, ?, ?, ?)";
        
        PreparedStatement ps = this.conn.prepareStatement(addMortgageQuery);
        
        ps.setString(1, contractor.getContractorID());
        ps.setString(2, contractor.getSpecification());
        ps.setString(3, contractor.getCompany());
        ps.setString(4, contractor.getPhone());
        ps.setString(5, contractor.getEmail());
        
        ps.executeUpdate();
    }    
    
    
    
    /**
     * databaseUpdate Method for updating object in the database
     * @param object
     * @param type
     * @throws SQLException 
     */
    public void databaseUpdate(Object object, String type) throws SQLException{
        switch(type){
            case "property":
                updateProperty((Property) object);
                break;
            case "mortgage":
                updateMortgage((Mortgage) object);
                break;
            case "expense":
                updateExpense((Expense) object);
                break;
            case "tenant":
                updateTenant((Tenant) object);
                break;
            case "contractor":
                updateContractor((Contractor) object);
                break;
        }
    }
    
    /**
     * updateProperty Method to update the data from the Property Object in the database
     * @param property
     * @throws SQLException 
     */
    private void updateProperty(Property property) throws SQLException{
    
        String updatePropertyQuery = "UPDATE propman_property " +
                                     "SET fee=?, full=? " +
                                     "WHERE property_id=?";
        
        PreparedStatement ps = this.conn.prepareStatement(updatePropertyQuery);
        
        ps.setDouble(1, property.getFee());
        ps.setString(2, property.getFull() + "");
        ps.setString(3, property.getPropertyID());
        
        ps.executeUpdate();
        
    }
    
    /**
     * updateMortgage Method to update the data from the Mortgage Object in the database
     * @param mortgage
     * @throws SQLException 
     */
    private void updateMortgage(Mortgage mortgage) throws SQLException{
    
        String updateMortgageQuery = "UPDATE propman_mortgage " +
                                     "SET bank=?, loan_Term=?, amount=?, interest_rate=?, monthly_payment=? " +
                                     "WHERE property_id=?";
        
        PreparedStatement ps = this.conn.prepareStatement(updateMortgageQuery);
        
        ps.setString(1, mortgage.getBank());
        ps.setInt(2, mortgage.getLoanTerm());
        ps.setDouble(3, mortgage.getAmount());
        ps.setDouble(4, mortgage.getInterestRate());
        ps.setDouble(5, mortgage.getMonthlyPayment());
        ps.setString(6, mortgage.getPropertyID());
        
        ps.executeUpdate();
        
    }
    
    /**
     * updateExpense Method to update the data from the Expense Object in the database
     * @param expense
     * @throws SQLException 
     */
    private void updateExpense(Expense expense) throws SQLException{
    
        String updateMortgageQuery = "UPDATE propman_expense " +
                                     "SET cost=? " +
                                     "WHERE expense_id=?";
        
        PreparedStatement ps = this.conn.prepareStatement(updateMortgageQuery);
        
        ps.setDouble(1, expense.getCost());
        ps.setString(2, expense.getExpenseID());
        
        ps.executeUpdate();
        
    }
  
    /**
     * updateTenant Method to update the data from the Tenant Object in the database
     * @param tenant
     * @throws SQLException 
     */
    private void updateTenant(Tenant tenant) throws SQLException{
    
        String updateMortgageQuery = "UPDATE propman_tenant " +
                                     "SET rent=?, paid_amount=?, renew=?, phone=?, email=?, lease=? " +
                                     "WHERE tenant_id=?";
        
        PreparedStatement ps = this.conn.prepareStatement(updateMortgageQuery);
        
        ps.setDouble(1, tenant.getTenantRent());
        ps.setDouble(2, tenant.getPaidAmount());
        ps.setString(3, tenant.getTenantRenew() + "");
        ps.setString(4, tenant.getTenantPhone());
        ps.setString(5, tenant.getTenantEmail());
        ps.setString(6, tenant.getLeasePath());
        ps.setString(7, tenant.getTenantID());
        
        ps.executeUpdate();
        
    }

    /**
     * updateContractor Method to update the data from the Contractor Object in the database
     * @param contractor
     * @throws SQLException 
     */
    private void updateContractor(Contractor contractor) throws SQLException{
    
        String updateMortgageQuery = "UPDATE propman_contractor " +
                                     "SET phone=?, email=? " +
                                     "WHERE contractor_id=?";
        
        PreparedStatement ps = this.conn.prepareStatement(updateMortgageQuery);
        
        ps.setString(1, contractor.getPhone());
        ps.setString(2, contractor.getEmail());
        ps.setString(3, contractor.getContractorID());
        
        ps.executeUpdate();
        
    }
    
    
    
    /**
     * databaseRemove Method for removing an object from the database
     * @param table
     * @param id_type
     * @param id_input
     * @throws SQLException 
     */
    public void databaseRemove(String table, String id_type, String id_input) throws SQLException{
        String queryDelete = "DELETE FROM " + table + " WHERE " + id_type + " = '" + id_input + "'";
        
        executeStatement(queryDelete);
    }
    
    
    
    /**
     * calculateSumExpenses Method that calculates the total expenses of the inputted property 
     * id -> total expenses of the property with that id
     * all -> total expenses of all of the properties
     * @param input
     * @return
     * @throws SQLException 
     */
    public double calculateSumExpenses(String input) throws SQLException{
        
        String sumExpensesQuery = "";
        double output = 0;
        PreparedStatement ps;
        
        if(input.toLowerCase().equals("all")){
            sumExpensesQuery = "SELECT SUM(cost) FROM propman_expense";
            ps = this.conn.prepareStatement(sumExpensesQuery);
        }
        
        else{
            sumExpensesQuery = "SELECT SUM(cost) FROM propman_expense " +
                               "WHERE property_id=?";
            ps = this.conn.prepareStatement(sumExpensesQuery);
            ps.setString(1, input);
        }
         
        ResultSet rs = ps.executeQuery();
        
        while(rs.next()){
            output = rs.getDouble(1);
        }
        
        return output;
    }
}


