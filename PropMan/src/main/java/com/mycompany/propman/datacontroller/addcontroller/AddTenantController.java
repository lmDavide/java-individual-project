/**
 * Controller for Adding a Tenant View (add_tenant)
 * @author David Tran - 1938381
 */

package com.mycompany.propman.datacontroller.addcontroller;

// Imports
import com.mycompany.propman.App;
import com.mycompany.propman.models.Tenant;

import javafx.fxml.FXML;
import java.sql.SQLException;
import javafx.scene.control.TextField;

public class AddTenantController extends AddController{
    
    // Fields
    public TextField tf_tenant_id;
    public TextField tf_property_id;
    public TextField tf_unit_number;
    public TextField tf_name;
    public TextField tf_rent;
    public TextField tf_paid;
    public TextField tf_renew;
    public TextField tf_phone;
    public TextField tf_email;
    public TextField tf_file;
    
    public Tenant tenant;
    
    
    
    /**
    * buttonAddObject Method that gets called when clicking the Add Tenant Button to add the data inputted to the table
     * @throws java.sql.SQLException
    */
    @Override
    public void buttonAddObject() throws SQLException{
        createObject();

        try{
            App.dbObject.databaseAdd(this.tenant, "tenant");

            this.table.getItems().add(this.tenant);  
        }
        catch(SQLException e){
            System.out.println("Error.");
        }
        
        super.closeStage("Tenant");
    }
    
    /**
     * createObject Method to create a Tenant Object
     */
    @Override
    public void createObject(){
        Tenant newTenant = new Tenant(tf_tenant_id.getText(), tf_property_id.getText(), Integer.parseInt(tf_unit_number.getText()), tf_name.getText(), Double.parseDouble(tf_rent.getText()), Double.parseDouble(tf_paid.getText()), Boolean.parseBoolean(tf_renew.getText()), tf_phone.getText(), tf_email.getText(), tf_file.getText());
        
        this.tenant = newTenant;
    }
    
    /**
     * addLease Method that gets called when clicking on Add Lease Button
     */
    @FXML
    public void addLease(){
        tf_file.setText(super.newLease("Add"));
    }
}
