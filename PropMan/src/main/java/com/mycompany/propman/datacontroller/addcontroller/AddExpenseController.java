/**
 * Controller for Adding an Expense View (add_expense)
 * @author David Tran - 1938381
 */

package com.mycompany.propman.datacontroller.addcontroller;

// Imports
import com.mycompany.propman.App;
import com.mycompany.propman.models.Expense;
import java.sql.SQLException;
import javafx.scene.control.TextField;

public class AddExpenseController extends AddController{
    
    // Fields
    public TextField tf_expense_id;
    public TextField tf_property_id;
    public TextField tf_type;
    public TextField tf_cost;
    
    public Expense expense;
    
    
    
    /**
    * buttonAddExpense Method that gets called when clicking the Add Expense Button to add the data inputted to the table
     * @throws java.sql.SQLException
    */
    @Override
    public void buttonAddObject() throws SQLException{
        createObject();
        
        try{
            App.dbObject.databaseAdd(this.expense, "expense");

            this.table.getItems().add(this.expense);
        }
        catch(SQLException e){
            System.out.println("Error.");
        }
        
        super.closeStage("Expense");
    }
    
    /**
     * createMortgage Method to create a Mortgage Object
     */
    @Override
    public void createObject(){
        Expense newExpense = new Expense(tf_expense_id.getText(), tf_property_id.getText(), tf_type.getText(), Double.parseDouble(tf_cost.getText()));
        this.expense = newExpense;
    }
}
