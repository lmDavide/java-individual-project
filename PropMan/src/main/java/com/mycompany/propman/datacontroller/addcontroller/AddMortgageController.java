/**
 * Controller for Adding a Mortgage View (add_mortgage)
 * @author David Tran - 1938381
 */

package com.mycompany.propman.datacontroller.addcontroller;

// Imports
import com.mycompany.propman.App;
import com.mycompany.propman.models.Mortgage;
import java.sql.SQLException;
import javafx.scene.control.TextField;

public class AddMortgageController extends AddController{
   
    // Fields
    public TextField tf_prop_id;
    public TextField tf_mort_id;
    public TextField tf_bank;
    public TextField tf_loan_term;
    public TextField tf_amount;
    public TextField tf_rate;
    
    public Mortgage mortgage;
    
    
    
   /**
    * buttonAddMortgage Method that gets called when clicking the Add Mortgage Button to add the data inputted to the table
     * @throws java.sql.SQLException
    */
   @Override
    public void buttonAddObject() throws SQLException{
        createObject();

        try{
            App.dbObject.databaseAdd(this.mortgage, "mortgage");
            
            this.table.getItems().add(this.mortgage);
        }
        catch(SQLException e){
            System.out.println("Error.");
        }
        
        super.closeStage("Mortgage");
        
    }
    
    /**
     * createMortgage Method to create a Mortgage Object
     */
    @Override
    public void createObject(){
        
        Mortgage newMortgage = new Mortgage(tf_prop_id.getText(), tf_mort_id.getText(), tf_bank.getText(), Integer.parseInt(tf_loan_term.getText()), Double.parseDouble(tf_amount.getText()), Double.parseDouble(tf_rate.getText()));
        
        this.mortgage = newMortgage;
    }
}
