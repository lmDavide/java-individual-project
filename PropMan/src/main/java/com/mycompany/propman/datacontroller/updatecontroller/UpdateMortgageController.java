/**
 * Controller for Updating a Mortgage View (update_mortgage)
 * @author David Tran - 1938381
 */

package com.mycompany.propman.datacontroller.updatecontroller;

// Imports
import com.mycompany.propman.App;
import com.mycompany.propman.models.Mortgage;
import java.sql.SQLException;
import javafx.scene.control.TextField;



public class UpdateMortgageController extends UpdateController{
    
    // Fields
    public TextField tf_prop_id;
    public TextField tf_mort_id;
    public TextField tf_bank;
    public TextField tf_loan_term;
    public TextField tf_amount;
    public TextField tf_rate;
    
    public Mortgage mortgage;
    
    
    
    /**
     * Setter for the Mortgage Object
     * @param selectedObject 
     */
    @Override
    public void setObject(Object selectedObject){
        this.mortgage = (Mortgage) selectedObject;
    }
    
    /**
     * showData Method puts the data of a selected Mortgage from the table into the TextFields
     */
    @Override
    public void showData(){     
        
        tf_prop_id.setText(this.mortgage.getPropertyID());
        tf_mort_id.setText(this.mortgage.getMortgageID());
        tf_bank.setText(this.mortgage.getBank());
        tf_loan_term.setText(this.mortgage.getLoanTerm() + "");
        tf_amount.setText(this.mortgage.getAmount()+ "");
        tf_rate.setText(this.mortgage.getInterestRate()+ "");
        
        tf_prop_id.setEditable(false);
        tf_mort_id.setEditable(false);
    }
    
    /**
     * buttonUpdateMortgage Method that gets called when clicking the Update Mortgage Button to update the data inputted to the table
     * @throws java.sql.SQLException
     */
    @Override
    public void updateData() throws SQLException{
        
        this.mortgage.setBank(tf_bank.getText());
        this.mortgage.setLoanTerm(Integer.parseInt(tf_loan_term.getText()));
        this.mortgage.setAmount(Double.parseDouble(tf_amount.getText()));
        this.mortgage.setInterestRate(Double.parseDouble(tf_rate.getText()));
        this.mortgage.setMonthlyPayment();
        
        this.table.refresh();
        
        App.dbObject.databaseUpdate(this.mortgage, "mortgage");
        
        this.stage.close();
    }
    
}

