/**
 * Controller for Updating an Expense View (update_expense)
 * @author David Tran - 1938381
 */

package com.mycompany.propman.datacontroller.updatecontroller;

// Imports
import com.mycompany.propman.App;
import com.mycompany.propman.models.Expense;
import java.sql.SQLException;
import javafx.scene.control.TextField;



public class UpdateExpenseController extends UpdateController{
    
    // Fields
    public TextField tf_expense_id;
    public TextField tf_property_id;
    public TextField tf_type;
    public TextField tf_cost;
    
    public Expense expense;
    
    /**
     * Setter for the Expense Object
     * @param selectedObject 
     */
    @Override
    public void setObject(Object selectedObject){
        this.expense = (Expense) selectedObject;
    }
    
    /**
     * setExpenseData Method puts the data of a selected Expense from the table into the TextFields
     */
    @Override
    public void showData(){
        
        tf_expense_id.setText(this.expense.getExpenseID());
        tf_property_id.setText(this.expense.getPropertyID());
        tf_type.setText(this.expense.getType());
        tf_cost.setText(this.expense.getCost()+ "");
        
        tf_expense_id.setEditable(false);
        tf_property_id.setEditable(false);
        tf_type.setEditable(false);
    }
    
    /**
     * buttonExpenseMortgage Method that gets called when clicking the Update Expense Button to update the data inputted to the table
     * @throws java.sql.SQLException
     */
    @Override
    public void updateData() throws SQLException{
        
        this.expense.setCost(Double.parseDouble(tf_cost.getText()));
        
        this.table.refresh();
        
        App.dbObject.databaseUpdate(this.expense, "expense");
        
        this.stage.close();
    }
}
