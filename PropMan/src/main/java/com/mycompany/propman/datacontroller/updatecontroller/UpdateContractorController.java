/**
 * Controller for Updating a Contractor View (update_contractor)
 * @author David Tran - 1938381
 */

package com.mycompany.propman.datacontroller.updatecontroller;

// Imports
import com.mycompany.propman.App;
import com.mycompany.propman.models.Contractor;
import java.sql.SQLException;
import javafx.scene.control.TextField;



public class UpdateContractorController extends UpdateController{
    
    // Fields
    public TextField tf_id;
    public TextField tf_specification;
    public TextField tf_company;
    public TextField tf_phone;
    public TextField tf_email;
    
    public Contractor contractor;
    
    
    
    /**
     * Setter for the Contractor Object
     * @param selectedObject 
     */
    @Override
    public void setObject(Object selectedObject){
        this.contractor = (Contractor) selectedObject;
    }
    
    /**
     * showData Method puts the data of a selected Contractor from the table into the TextFields
     */
    @Override
    public void showData(){

        tf_id.setText(this.contractor.getContractorID());
        tf_specification.setText(this.contractor.getSpecification());
        tf_company.setText(this.contractor.getCompany());
        tf_phone.setText(this.contractor.getPhone()+ "");
        tf_email.setText(this.contractor.getEmail());
        
        tf_id.setEditable(false);
        tf_specification.setEditable(false);
        tf_company.setEditable(false);
        
    }
    
    /**
     * updateData Method that gets called when clicking the Update Contractor Button to update the data inputted to the table
     * @throws java.sql.SQLException
     */
    @Override
    public void updateData() throws SQLException{
        
        this.contractor.setPhone(tf_phone.getText());
        this.contractor.setEmail(tf_email.getText());
        
        this.table.refresh();
        
        App.dbObject.databaseUpdate(this.contractor, "contractor");
        
        this.stage.close();
    }
}
