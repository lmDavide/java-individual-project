/**
 * Controller for Adding a Property View (add_property)
 * @author David Tran - 1938381
 */

package com.mycompany.propman.datacontroller.addcontroller;

// Imports
import com.mycompany.propman.App;
import com.mycompany.propman.models.Condo;
import com.mycompany.propman.models.House;
import com.mycompany.propman.models.Plex;
import com.mycompany.propman.models.Property;
import java.sql.SQLException;
import javafx.scene.control.TextField;

public class AddPropertyController extends AddController{
    
    // Fields
    public TextField tf_id;
    public TextField tf_type;
    public TextField tf_address;
    public TextField tf_units;
    public TextField tf_fee;
    public TextField tf_full;
    
    public Property property;
   
    
    
   /**
    * buttonAddObject Method that gets called when clicking the Add Property Button to add the data inputted to the table
     * @throws java.sql.SQLException
    */
   @Override
    public void buttonAddObject() throws SQLException{
        createObject();
        
        try{
            App.dbObject.databaseAdd(this.property, "property");

            this.table.getItems().add(this.property);
        }
        catch(SQLException e){
            System.out.println("Error.");
        }
        
        super.closeStage("Property");
        
    }
    
    /**
     * Creates a Property Object based on the Property Type
     */
    @Override
    public void createObject(){        
        String type_input = tf_type.getText().toLowerCase();
        boolean full_input = Boolean.parseBoolean(tf_full.getText().toLowerCase());
        
        switch (type_input) {
            case "house":
                {
                    House newProperty = new House(tf_id.getText(), tf_address.getText(), full_input);
                    this.property = newProperty;
                    break;
                }
            case "condo":
                {
                    Condo newProperty = new Condo(tf_id.getText(), tf_address.getText(), Double.parseDouble(tf_fee.getText()), full_input);
                    this.property = newProperty;
                    break;
                }
            default: // Plexes
                {
                    String plex_type = type_input.substring(0, 1).toUpperCase() + type_input.substring(1);
                    Plex newProperty = new Plex(tf_id.getText(), plex_type, tf_address.getText(), Integer.parseInt(tf_units.getText()), full_input);
                    this.property = newProperty;
                    break;
                }
        }
    }
    
}
