/**
 * Controller for Updating a Tenant View (update_tenant)
 * @author David Tran - 1938381
 */

package com.mycompany.propman.datacontroller.updatecontroller;

// Imports
import com.mycompany.propman.App;
import com.mycompany.propman.models.Tenant;

import javafx.fxml.FXML;
import java.sql.SQLException;
import javafx.scene.control.TextField;



public class UpdateTenantController extends UpdateController{
    
    // Fields
    public TextField tf_tenant_id;
    public TextField tf_property_id;
    public TextField tf_unit_number;
    public TextField tf_name; 
    public TextField tf_rent;
    public TextField tf_paid;
    public TextField tf_renew;
    public TextField tf_phone;
    public TextField tf_email;
    public TextField tf_file;
    
    public Tenant tenant;
    
    
    
    /**
     * Setter for the Tenant Object
     * @param selectedObject 
     */
    @Override
    public void setObject(Object selectedObject){
        this.tenant = (Tenant) selectedObject;
    }
    
    /**
     * showData Method puts the data of the selected Tenant from the table into the TextFields
     */
    @Override
    public void showData(){
        
        tf_tenant_id.setText(this.tenant.getTenantID());
        tf_property_id.setText(this.tenant.getPropertyID());
        tf_unit_number.setText(this.tenant.getUnitNumber() + "");
        tf_name.setText(this.tenant.getTenantName());
        tf_rent.setText(this.tenant.getTenantRent()+ "");
        tf_paid.setText(this.tenant.getPaidAmount()+ "");
        tf_renew.setText(this.tenant.getTenantRenew()+ "");
        tf_phone.setText(this.tenant.getTenantPhone()+ "");
        tf_email.setText(this.tenant.getTenantEmail() + "");
        tf_file.setText(this.tenant.getLeasePath());
        
        tf_tenant_id.setEditable(false);
        tf_property_id.setEditable(false);
        tf_unit_number.setEditable(false);
        tf_name.setEditable(false); 
    }
    
    /**
     * updateData Method that gets called when clicking the Update Property Button to update the data inputted to the table
     * @throws java.sql.SQLException
     */
    @Override
    public void updateData() throws SQLException{
        
        this.tenant.SetTenantRent(Double.parseDouble(tf_rent.getText()));
        this.tenant.setPaidAmount(Double.parseDouble(tf_paid.getText()));
        this.tenant.setTenantRenew(Boolean.parseBoolean(tf_renew.getText()));
        this.tenant.setTenantPhone(tf_phone.getText());
        this.tenant.setTenantEmail(tf_email.getText());
        this.tenant.setLeasePath(tf_file.getText());
        
        this.table.refresh();
        
        App.dbObject.databaseUpdate(this.tenant, "tenant");
        
        this.stage.close();
    }
    
    /**
     * changeLease Method that gets called when clicking on Change Lease Button
     */
    @FXML
    public void changeLease(){
        tf_file.setText(super.newLease("Update"));
    }
}
