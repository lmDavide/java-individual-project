/**
 * Superclass for the AddControllers (AddPropertyController, AddMortgageController, AddExpenseController, AddTenantController, AddContractorController)
 * @author David Tran - 1938381
 */

package com.mycompany.propman.datacontroller.addcontroller;

// Imports
import com.mycompany.propman.datacontroller.DataController;
import java.sql.SQLException;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public abstract class AddController extends DataController{
    
    /**
     * buttonAddObject Method for each AddController that adds data to the table
     * @throws java.sql.SQLException
     */
    public abstract void buttonAddObject() throws SQLException;
    
    /**
     * createObject for each AddController to create their respective object
     */
    public abstract void createObject();
  
    public void closeStage(String currentData){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Add another " + currentData + " ?", ButtonType.YES, ButtonType.NO);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.NO) {
            this.stage.close();  
        }
    }
}
