/**
 * Controller for Adding a Contractor View (add_contractor)
 * @author David Tran - 1938381
 */

package com.mycompany.propman.datacontroller.addcontroller;

// Imports
import com.mycompany.propman.App;
import com.mycompany.propman.models.Contractor;
import java.sql.SQLException;
import javafx.scene.control.TextField;

public class AddContractorController extends AddController{
    
    // Fields
    public TextField tf_id;
    public TextField tf_specification;
    public TextField tf_company;
    public TextField tf_phone;
    public TextField tf_email;
    
    public Contractor contractor;
    
    
    
    /**
    * buttonAddObject Method that gets called when clicking the Add Contractor Button to add the data inputted to the table
     * @throws java.sql.SQLException
    */
    @Override
    public void buttonAddObject() throws SQLException{
        createObject();
        
        try{
            App.dbObject.databaseAdd(this.contractor, "contractor");

            this.table.getItems().add(this.contractor);
        }
        catch(SQLException e){
            System.out.println("Error.");
        }
        
        super.closeStage("Contractor");
    }
    
    /**
     * createObject Method to create a Contractor Object
     */
    @Override
    public void createObject(){
        Contractor newContractor = new Contractor(tf_id.getText(), tf_specification.getText(), tf_company.getText(), tf_phone.getText(), tf_email.getText());
        
        this.contractor = newContractor;
    }
}
