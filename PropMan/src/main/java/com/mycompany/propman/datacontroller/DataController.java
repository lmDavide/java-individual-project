/**
 * Superclass for the DataControllers (PropertyDataController, MortgageDataController, ExpenseDataController, TenantDataController, ContractorDataController)
 * @author David Tran - 1938381
 */

package com.mycompany.propman.datacontroller;

// Imports
import java.io.File;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class DataController {
    protected TableView table;
    protected Stage stage;
    
    /**
     * setTableView Method acts as a setter to store a TableView for the Controller
     * @param tableInput 
     */
    public void setTableView(TableView tableInput){
        this.table = tableInput;
    }
    
    /**
     * setScene Method acts as a setter to store the Stage for the Controller
     * @param stageInput 
     */
    public void setStage(Stage stageInput){
        this.stage = stageInput;
    }
    
    /**
     * newLease Method that opens a File Chooser to save the Lease Path
     * @param keyword
     * @return 
     */
    @FXML
    public String newLease(String keyword){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(keyword + " Lease");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PDF", "*.pdf"));
        File selectedFile = fileChooser.showOpenDialog(stage);
        //tf_file.setText(selectedFile.getAbsolutePath());
        return selectedFile.getAbsoluteFile() + "";
    }
}