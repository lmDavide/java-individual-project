/**
 * Superclass for the UpdateControllers (UpdatePropertyController, UpdateMortgageController, UpdateExpenseController, UpdateTenantController, UpdateContractorController)
 * @author David Tran - 1938381
 */

package com.mycompany.propman.datacontroller.updatecontroller;

// Imports
import com.mycompany.propman.datacontroller.DataController;
import java.sql.SQLException;



public abstract class UpdateController extends DataController{
    
    public abstract void setObject(Object selectedObject);
    
    public abstract void showData();
    
    public abstract void updateData() throws SQLException;
}
