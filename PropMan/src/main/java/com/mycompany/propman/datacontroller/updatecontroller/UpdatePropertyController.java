/**
 * Controller for Updating a Property View (update_property)
 * @author David Tran - 1938381
 */

package com.mycompany.propman.datacontroller.updatecontroller;

// Imports
import com.mycompany.propman.App;
import com.mycompany.propman.models.Property;
import java.sql.SQLException;
import javafx.scene.control.TextField;



public class UpdatePropertyController extends UpdateController {
    public Property property;
    
    public TextField tf_id;
    public TextField tf_type;
    public TextField tf_address;
    public TextField tf_units;
    public TextField tf_fee;
    public TextField tf_full;
    
    
    
    /**
     * Setter for the Property Object
     * @param selectedObject 
     */
    @Override
    public void setObject(Object selectedObject){
        this.property = (Property) selectedObject;
    }
    
    /**
     * showData Method puts the data of the selected Property from the table into the TextFields
     */
    @Override
    public void showData(){
        
        tf_id.setText(this.property.getPropertyID());
        tf_type.setText(this.property.getType());
        tf_address.setText(this.property.getAddress());
        tf_units.setText(this.property.getUnits() + "");
        tf_fee.setText(this.property.getFee() + "");
        tf_full.setText(this.property.getFull() + "");
        
        tf_id.setEditable(false);
        tf_type.setEditable(false);
        tf_address.setEditable(false);
        tf_units.setEditable(false);
    }
    
    /**
     * buttonUpdateProperty Method that gets called when clicking the Update Property Button to update the data inputted to the table
     * @throws java.sql.SQLException
     */
    @Override
    public void updateData() throws SQLException{
        
        this.property.setFee(Double.parseDouble(tf_fee.getText()));
        this.property.setFull(Boolean.parseBoolean(tf_full.getText()));
        
        this.table.refresh();
        
        App.dbObject.databaseUpdate(this.property, "property");    
        
        this.stage.close();
    }

}
