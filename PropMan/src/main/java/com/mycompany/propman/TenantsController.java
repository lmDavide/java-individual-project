package com.mycompany.propman;

// Imports
import com.mycompany.propman.models.Tenant;
import com.mycompany.propman.datacontroller.addcontroller.AddTenantController;
import com.mycompany.propman.datacontroller.updatecontroller.UpdateTenantController;

import java.awt.Desktop;
import java.io.IOException;
import java.io.File;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;





public class TenantsController extends MainController implements Initializable {
    
    // Fields
    @FXML TableView<Tenant> tenantTable;
    
    @FXML TableColumn<Tenant, String> tenant_id;
    @FXML TableColumn<Tenant, String> property_id;
    @FXML TableColumn<Tenant, Integer> unit_number;
    @FXML TableColumn<Tenant, String> tenant_name;
    @FXML TableColumn<Tenant, Double> tenant_rent;
    @FXML TableColumn<Tenant, Double> paid_amount;
    @FXML TableColumn<Tenant, Boolean> paid_rent;
    @FXML TableColumn<Tenant, Boolean> tenant_renew;
    @FXML TableColumn<Tenant, String> tenant_phone;
    @FXML TableColumn<Tenant, String> tenant_email;
    @FXML TableColumn<Tenant, String> tenant_lease;
    
    public ObservableList<Tenant> observableList;
    public List<Tenant> list;

    
        
    /**
     * addTenant Method that gets called when Add Tenant Button is clicked
     * @throws IOException 
     */
    @FXML 
    @Override
    public void add() throws IOException {
        AddTenantController controller = new AddTenantController();
        super.newView(this.tenantTable, controller, "/com/mycompany/propman/add_tenant.fxml", "Add Tenant", 650);
    }
    
    /**
     * updateTenant Method that gets called when Update Tenant Button is clicked
     * @throws IOException 
     */
    @FXML 
    @Override
    public void update() throws IOException {
        if(!tenantTable.getSelectionModel().isEmpty()){
            Tenant selectedTenant = tenantTable.getSelectionModel().getSelectedItem();

            UpdateTenantController controller = new UpdateTenantController();

            super.newView(this.tenantTable, controller, "/com/mycompany/propman/update_tenant.fxml", "Update Tenant", 650);

            controller.setObject(selectedTenant);
            controller.showData();
        }
    }
    
    /**
     * removeTenant Method that gets called when Remove Tenant Button is clicked
     * @throws IOException 
     * @throws java.sql.SQLException 
     */
    @FXML 
    @Override
    public void remove() throws IOException, SQLException {
        if(!tenantTable.getSelectionModel().isEmpty()){
            Tenant selectedTenant = tenantTable.getSelectionModel().getSelectedItem();
            String removed_id = selectedTenant.getTenantID();

            super.removeData("Tenant", selectedTenant, tenantTable, "propman_tenant", "tenant_id", removed_id);
        }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources){
        tenant_id.setCellValueFactory(new PropertyValueFactory<>("TenantID"));
        property_id.setCellValueFactory(new PropertyValueFactory<>("PropertyID"));
        unit_number.setCellValueFactory(new PropertyValueFactory<>("UnitNumber"));
        tenant_name.setCellValueFactory(new PropertyValueFactory<>("TenantName"));
        tenant_rent.setCellValueFactory(new PropertyValueFactory<>("TenantRent"));
        paid_amount.setCellValueFactory(new PropertyValueFactory<>("PaidAmount"));
        paid_rent.setCellValueFactory(new PropertyValueFactory<>("RentPaid"));
        tenant_renew.setCellValueFactory(new PropertyValueFactory<>("TenantRenew"));
        tenant_phone.setCellValueFactory(new PropertyValueFactory<>("TenantPhone"));
        tenant_email.setCellValueFactory(new PropertyValueFactory<>("TenantEmail"));
        tenant_lease.setCellValueFactory(new PropertyValueFactory<>("LeasePath"));
        
        try {
            this.observableList = fetchData("tenant");
        } catch (SQLException ex) {
            Logger.getLogger(PropertiesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        tenantTable.setItems(this.observableList);
        
        flagTenant();
    }   
   
    /**
     * Function for setting up the row colors based on the status of the rent of the tenant (paid or not paid)
     */
    private void flagTenant(){
        tenantTable.setRowFactory(tv -> new TableRow<Tenant>() {
            @Override
            public void updateItem(Tenant tenant, boolean empty) {
                super.updateItem(tenant, empty) ;
                if (tenant == null) {
                    setStyle("");
                } 
                
                else if (tenant.getRentPaid() == false) {
                    setStyle("-fx-background-color: salmon;");
                } 
                
                else{
                    setStyle("-fx-background-color: lightgreen;");
                }
            }
        });
    }
    
        
    @FXML 
    private void showLease() throws IOException {
        if(!tenantTable.getSelectionModel().isEmpty()){
            
            File file = new File(tenantTable.getSelectionModel().getSelectedItem().getLeasePath());
            
            Desktop.getDesktop().open(file);
        }
    }
    
}
