/**
 * Controller for Mortgages View (mortgages_main)
 * @author David Tran - 1938381
 */

package com.mycompany.propman;

// Imports
import com.mycompany.propman.models.Mortgage;
import com.mycompany.propman.datacontroller.addcontroller.AddMortgageController;
import com.mycompany.propman.datacontroller.updatecontroller.UpdateMortgageController;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;



public class MortgagesController extends MainController implements Initializable {
    
    // Fields
    @FXML public TableView<Mortgage> mortgageTable;    
        
    @FXML public TableColumn<Mortgage, String> property_id;
    @FXML public TableColumn<Mortgage, String> mortgage_id;
    @FXML public TableColumn<Mortgage, String> mortgage_bank;
    @FXML public TableColumn<Mortgage, Integer> loan_term;
    @FXML public TableColumn<Mortgage, Double> mortgage_amount;
    @FXML public TableColumn<Mortgage, Double> interest_rate;
    @FXML public TableColumn<Mortgage, Double> monthly_payment;

    public ObservableList<Mortgage> observableList;
    public List<Mortgage> list;
    
    
    
    /**
     * addMortgage Method that gets called when Add Mortgage Button is clicked
     * @throws IOException 
     */
    @FXML 
    @Override
    public void add() throws IOException {
        AddMortgageController controller = new AddMortgageController();
        
        super.newView(this.mortgageTable, controller, "/com/mycompany/propman/add_mortgage.fxml", "Add Mortgage", 480);
    }
    
    /**
     * updateMortgage Method that gets called when Update Mortgage Button is clicked
     * @throws IOException 
     */
    @FXML 
    @Override
    public void update() throws IOException {
        if(!mortgageTable.getSelectionModel().isEmpty()){
            Mortgage selectedMortgage = mortgageTable.getSelectionModel().getSelectedItem();

            UpdateMortgageController controller = new UpdateMortgageController();

            super.newView(this.mortgageTable, controller, "/com/mycompany/propman/update_mortgage.fxml", "Update Mortgage", 480);

            controller.setObject(selectedMortgage);
            controller.showData();
        }
    }
    
    /**
     * removeMortgage Method that gets called when Remove Mortgage Button is clicked
     * @throws java.sql.SQLException
     * @throws IOException 
     */
    @FXML 
    @Override
    public void remove() throws IOException, SQLException{
        if(!mortgageTable.getSelectionModel().isEmpty()){
            Mortgage selectedMortgage = mortgageTable.getSelectionModel().getSelectedItem();
            String removed_id = selectedMortgage.getPropertyID();

            super.removeData("Mortgage", selectedMortgage, mortgageTable, "propman_mortgage", "property_id", removed_id);
        }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources){
        property_id.setCellValueFactory(new PropertyValueFactory<>("PropertyID"));
        mortgage_id.setCellValueFactory(new PropertyValueFactory<>("MortgageID"));
        mortgage_bank.setCellValueFactory(new PropertyValueFactory<>("Bank"));
        loan_term.setCellValueFactory(new PropertyValueFactory<>("LoanTerm"));
        mortgage_amount.setCellValueFactory(new PropertyValueFactory<>("Amount"));
        interest_rate.setCellValueFactory(new PropertyValueFactory<>("InterestRate"));
        monthly_payment.setCellValueFactory(new PropertyValueFactory<>("MonthlyPayment"));

        try {
            this.observableList = super.fetchData("mortgage");
        } catch (SQLException ex) {
            Logger.getLogger(PropertiesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        mortgageTable.setItems(this.observableList);
    }
    
}
