/**
 * Controller for Expenses View (expenses_main)
 * @author David Tran - 1938381
 */

package com.mycompany.propman;

// Imports
import com.mycompany.propman.models.Expense;
import com.mycompany.propman.datacontroller.addcontroller.AddExpenseController;
import com.mycompany.propman.datacontroller.updatecontroller.UpdateExpenseController;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;



public class ExpensesController extends MainController implements Initializable {
    
    // Fields
    @FXML TableView<Expense> expenseTable;
    
    @FXML TableColumn<Expense, String> expense_id;
    @FXML TableColumn<Expense, String> property_id;
    @FXML TableColumn<Expense, String> expense_type;
    @FXML TableColumn<Expense, Double> expense_cost;
    
    @FXML TextField property_id_input;
    @FXML TextField total_expenses_output;
    
    public ObservableList<Expense> observableList;
    public List<Expense> list;
    
    
    
    /**
     * addExpense Method that gets called when Add Expense Button is clicked
     * @throws IOException 
     */
    @FXML 
    @Override
    public void add() throws IOException {
        AddExpenseController controller = new AddExpenseController();
        
        super.newView(this.expenseTable, controller, "/com/mycompany/propman/add_expense.fxml", "Add Expense", 480);
    }
    
    /**
     * updateExpense Method that gets called when Update Expense Button is clicked
     * @throws IOException 
     */
    @FXML
    @Override
    public void update() throws IOException {
        if(!expenseTable.getSelectionModel().isEmpty()){
            Expense selectedExpense = expenseTable.getSelectionModel().getSelectedItem();

            UpdateExpenseController controller = new UpdateExpenseController();

            super.newView(this.expenseTable, controller, "/com/mycompany/propman/update_expense.fxml", "Add Expense", 480);

            controller.setObject(selectedExpense);
            controller.showData();
        }
    }
    
    /**
     * removeExpense Method that gets called when Remove Expense Button is clicked
     * @throws IOException 
     * @throws java.sql.SQLException 
     */
    @FXML 
    @Override
    public void remove() throws IOException, SQLException {
        if(!expenseTable.getSelectionModel().isEmpty()){
            Expense selectedExpense = expenseTable.getSelectionModel().getSelectedItem();
            String removed_id = selectedExpense.getExpenseID();

            super.removeData("Expense", selectedExpense, expenseTable, "propman_expense", "expense_id", removed_id);
        }
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources){
        expense_id.setCellValueFactory(new PropertyValueFactory<>("ExpenseID"));
        property_id.setCellValueFactory(new PropertyValueFactory<>("PropertyID"));
        expense_type.setCellValueFactory(new PropertyValueFactory<>("Type"));
        expense_cost.setCellValueFactory(new PropertyValueFactory<>("Cost"));

        try {
            this.observableList = fetchData("expense");
        } catch (SQLException ex) {
            Logger.getLogger(PropertiesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        expenseTable.setItems(this.observableList);
    }

    /**
     * calculateTotalExpenses Method that calls calls calculateSumExpenses when the Calculate Total is called
     * @throws SQLException 
     */
    @FXML
    public void calculateTotalExpenses() throws SQLException{
        total_expenses_output.setText(App.dbObject.calculateSumExpenses(property_id_input.getText()) + "");
    }
    
}
