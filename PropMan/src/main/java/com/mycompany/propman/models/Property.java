/**
 * Property Class
 * @author David Tran - 1938381
 */

package com.mycompany.propman.models;

public class Property {
    
    // Fields
    protected String property_id;
    protected String type;
    protected String address;
    protected int units;
    protected double fee;
    protected boolean full;
    
    // Getters Methods
    public String getPropertyID(){
        return this.property_id;
    }
    
    public String getType(){
        return this.type;
    }
    
    public String getAddress(){
        return this.address;
    }
    
    public int getUnits(){
        return this.units;
    }
    
    public double getFee(){
        return this.fee;
    }
    
    public boolean getFull(){
        return this.full;
    }
    
    
    
    // Setters Methods
    public void setPropertyID(String property_id){
        this.property_id = property_id;
    }
    
    public void setType(String type){
         this.type = type;
    }
    
    public void setAddress(String address){
        this.address = address;
    }
    
    public void setUnits(int units){
        this.units = units;
    }
    
    public void setFee(Double fee){
        this.fee = fee;
    }
    
    public void setFull(Boolean full){
        this.full = full;
    }
}
