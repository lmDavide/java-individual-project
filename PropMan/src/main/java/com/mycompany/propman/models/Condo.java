/**
 * Property Subclass - Condo Class
 * @author David Tran - 1938381
 */

package com.mycompany.propman.models;

public class Condo extends Property{
    
    public Condo(String property_id, String address, Double fee, boolean full){
        this.property_id = property_id;
        this.type = "Condo";
        this.address = address;
        this.units = 1;
        this.fee = fee;
        this.full = full;
    }
}

