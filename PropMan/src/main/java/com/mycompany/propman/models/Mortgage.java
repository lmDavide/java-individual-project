/**
 * Mortgage Class
 * @author David Tran - 1938381
 */

package com.mycompany.propman.models;

public class Mortgage {
    
    // Fields
    private String property_id;
    private String mortgage_id;
    private String bank;
    private int loan_term;
    private double amount;
    private double interest_rate;
    private double monthly_payment;
    
    /**
     * Mortgage Constructor
     * @param property_id
     * @param bank
     * @param loan_term
     * @param amount
     * @param interest_rate 
     */
    public Mortgage(String property_id, String mortgage_id, String bank, int loan_term, double amount, double interest_rate){
        this.property_id = property_id;
        this.mortgage_id = mortgage_id;
        this.bank = bank;
        this.loan_term = loan_term;
        this.amount = amount;
        this.interest_rate = interest_rate;
        this.monthly_payment = calculateMonthlyInterest(this.amount, this.loan_term, this.interest_rate);
    }
    
    
    
    // Getters Methods
    public String getPropertyID(){
        return this.property_id;
    }
    
    public String getMortgageID(){
        return this.mortgage_id;
    }
    
    public String getBank(){
        return this.bank;
    }
    
    
    public int getLoanTerm(){
        return this.loan_term;
    }
    
    public double getAmount(){
        return this.amount;
    }
    
    public double getInterestRate(){
        return this.interest_rate;
    }
    
    public double getMonthlyPayment(){
        return this.monthly_payment;
    }
    
    
    
    // Setters Methods
    public void setPropertyID(String property_id){
        this.property_id = property_id;
    }
    
    public void setMortgageID(String mortgage_id){
        this.mortgage_id = mortgage_id;
    }
    
    public void setBank(String bank){
         this.bank = bank;
    }
    
    public void setLoanTerm(int loan_term){
        this.loan_term = loan_term;
    }
    
    public void setAmount(double amount){
        this.amount = amount;
    }
    
    public void setInterestRate(double interest_rate){
        this.interest_rate = interest_rate;
    }
    
    public void setMonthlyPayment(){
        this.monthly_payment = calculateMonthlyInterest(this.amount, this.loan_term, this.interest_rate);
    }
    
    
    
    /**
     * calculateMonthlyInterest Method to calculate the Monthly Interest
     * @param mortgage_amount
     * @param years
     * @param interest_rate
     * @return 
     */
    private double calculateMonthlyInterest(double mortgage_amount, int years, double interest_rate){
        double months = years/12;
        double monthly_amount;
        
        monthly_amount = mortgage_amount*(interest_rate * Math.pow(1+interest_rate, months)/(Math.pow(1 + interest_rate, months) - 1));
        
        return monthly_amount;
    }
}
