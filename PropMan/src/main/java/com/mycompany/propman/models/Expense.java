/**
 * Expense Class
 * @author David Tran - 1938381
 */

package com.mycompany.propman.models;

public class Expense {
    
    // Fields
    private String expense_id;
    private String property_id;
    private String type;
    private double cost;
    
    /**
     * Expense Constructor
     * @param expense_id
     * @param property_id
     * @param type
     * @param cost 
     */
    public Expense(String expense_id, String property_id, String type, double cost){
        this.expense_id = expense_id;
        this.property_id = property_id;
        this.type = type;
        this.cost = cost;
    }
    
    
    
    // Getters Methods
    public String getExpenseID(){
        return this.expense_id;
    }
    
    public String getPropertyID(){
        return this.property_id;
    }
    
    public String getType(){
        return this.type;
    }
    
    public double getCost(){
        return this.cost;
    }
    
    
    
    // Setters Methods
    public void setExpenseID(String expense_id){
        this.expense_id = expense_id;
    }
    
    public void setPropertyID(String property_id){
        this.property_id = property_id;
    }
    
    public void setType(String type){
        this.type = type;
    }
    
    public void setCost(double cost){
        this.cost = cost;
    }
}
