/**
 * Contractor Class
 * @author David Tran - 1938381
 */

package com.mycompany.propman.models;

public class Contractor {
    
    // Fields
    private String contractor_id;
    private String specification;
    private String company;
    private String phone;
    private String email;
    
    /**
     * Contractor Constructor
     * @param contractor_id
     * @param specification
     * @param company
     * @param phone
     * @param email 
     */
    public Contractor(String contractor_id, String specification, String company, String phone, String email){
        this.contractor_id = contractor_id;
        this.specification = specification;
        this.company = company;
        this.phone = phone;
        this.email = email;
    }
    
    
    
    // Getters Methods
    public String getContractorID(){
        return this.contractor_id;
    }
    
    public String getSpecification(){
        return this.specification;
    }
        
    public String getCompany(){
        return this.company;
    }
    
    public String getPhone(){
        return this.phone;
    }
    
    public String getEmail(){
        return this.email;
    }
    
    
    
    // Setters Methods
    public void setContractorID(String contractor_id){
        this.contractor_id = contractor_id;
    }
    
    public void setSpecification(String specification){
        this.specification = specification;
    }
    
    public void setCompany(String company){
        this.company = company;
    }
    
    public void setPhone(String phone){
        this.phone = phone;
    }
    
    public void setEmail(String email){
        this.email = email;
    }
}
