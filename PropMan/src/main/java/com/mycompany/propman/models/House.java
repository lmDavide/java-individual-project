/**
 * Property Subclass - House Class
 * @author David Tran - 1938381
 */

package com.mycompany.propman.models;

public class House extends Property{
    
    public House(String property_id, String address, boolean full){
        this.property_id = property_id;
        this.type = "House";
        this.address = address;
        this.units = 1;
        this.fee = 0.0;
        this.full = full;
    }
}
