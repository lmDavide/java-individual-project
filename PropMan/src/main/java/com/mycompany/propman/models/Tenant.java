/**
 * Tenant Class
 * @author David Tran - 1938381
 */

package com.mycompany.propman.models;

public class Tenant {
    
    // Fields
    private String tenant_id;
    private String property_id;
    private int unit_number;
    private String tenant_name;
    private double tenant_rent;
    private double paid_amount;
    private boolean rent_paid;
    private boolean tenant_renew;
    private String tenant_phone;
    private String tenant_email;
    private String lease_path;
    
   
    
    /**
     * Tenant Constructor
     * @param tenant_id
     * @param property_id
     * @param unit_number
     * @param tenant_name
     * @param tenant_phone
     * @param paid_amount
     * @param tenant_email
     * @param tenant_rent
     * @param lease_path
     * @param tenant_renew 
     */
    public Tenant(String tenant_id, String property_id, int unit_number, String tenant_name, double tenant_rent, Double paid_amount, boolean tenant_renew, String tenant_phone, String tenant_email, String lease_path){
        this.tenant_id = tenant_id;
        this.property_id = property_id;
        this.unit_number = unit_number;
        this.tenant_name = tenant_name;
        this.tenant_rent = tenant_rent;
        this.paid_amount = paid_amount;
        this.rent_paid = checkPaid();
        this.tenant_renew = tenant_renew;
        this.tenant_phone = tenant_phone;
        this.tenant_email = tenant_email;
        this.lease_path = lease_path;
    }
    
    // Getters Methods
    public String getTenantID(){
        return this.tenant_id;
    }
    
    public String getPropertyID(){
        return this.property_id;
    }
    
    public int getUnitNumber(){
        return this.unit_number;
    }
    
    public String getTenantName(){
        return this.tenant_name;
    }
    
    public double getTenantRent(){
        return this.tenant_rent;
    }
    
    public double getPaidAmount(){
        return this.paid_amount;
    }
    
    public boolean getRentPaid(){
        return this.rent_paid;
    }
    
    public boolean getTenantRenew(){
        return this.tenant_renew;
    }
    
    public String getTenantPhone(){
        return this.tenant_phone;
    }
    
    public String getTenantEmail(){
        return this.tenant_email;
    }
    
    public String getLeasePath(){
        return this.lease_path;
    }
    
    
    
    // Setters Methods
    public void setTenantID(String tenant_id){
        this.tenant_id = tenant_id;
    }
    
    public void setPropertyID(String property_id){
        this.property_id = property_id;
    }
    
    public void setUnitNumber(int unit_number){
        this.unit_number = unit_number;
    }
    
    public void setTenantName(String tenant_name){
        this.tenant_name = tenant_name;
    }
    
    public void SetTenantRent(double tenant_rent){
        this.tenant_rent = tenant_rent;
    }
    
    public void setPaidAmount(double paid_amount){
        this.paid_amount = paid_amount;
    }
    
    public void setTenantRenew(boolean tenant_renew){
        this.tenant_renew = tenant_renew;
    }
    
    public void setTenantPhone(String tenant_phone){
        this.tenant_phone = tenant_phone;
    }
    
    public void setTenantEmail(String tenant_email){
        this.tenant_email = tenant_email;
    }
    
    public void setLeasePath(String lease_path){
        this.lease_path = lease_path;
    }
    
    
    /**
     * checkPaid Method that checks if the tenant paid their rent 
     * True if paid equal or more than the rent
     * False if paid less than the rent
     * @return 
     */
    private boolean checkPaid(){
        return this.tenant_rent <= this.paid_amount;
    }
}
