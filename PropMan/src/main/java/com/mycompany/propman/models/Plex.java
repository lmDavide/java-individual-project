/**
 * Property Subclass - House Class
 * @author David Tran - 1938381
 */

package com.mycompany.propman.models;

public class Plex extends Property{
    
    public Plex (String property_id, String type, String address, int units, boolean full){
        this.property_id = property_id;
        this.type = type;
        this.address = address;
        this.units = units;
        this.fee = 0.0;
        this.full = full;
    }    
}
