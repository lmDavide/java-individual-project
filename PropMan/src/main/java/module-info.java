module com.mycompany.propman {
    requires java.desktop;
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    

    opens com.mycompany.propman to javafx.fxml;
    exports com.mycompany.propman;
    exports com.mycompany.propman.database;
    exports com.mycompany.propman.datacontroller;
    exports com.mycompany.propman.datacontroller.addcontroller;
    exports com.mycompany.propman.datacontroller.updatecontroller;
    exports com.mycompany.propman.models;
}
